import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { CatListingComponent } from '../cat-listing/cat-listing.component';
import { HeaderComponent } from '../../app/shared/component/header.component';

import { UtilityService } from '../../app/shared/services/utility.service';
import { ApiService } from '../../app/shared/services/api.service';
import { FadeTransition } from '../../app/shared/transition/fade-transition.component';

@Component({
  selector: 'page-tv',
  templateUrl: 'tv.html'
})
export class TvPage {

  constructor(
    public navCtrl: NavController,
    public apiService: ApiService,
    public utilityService: UtilityService
  ) {}

  tabName: string = 'tv';
  cats: string[] = this.utilityService.defaults.cats[this.tabName];

  public itemSelected(item): void {
    this.navCtrl.push(CatListingComponent, { item, tab: this.tabName }, { animation: 'fade-animation', direction: 'forward' });
  }

}
