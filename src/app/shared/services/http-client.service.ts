import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { UtilityService } from './utility.service';
import { LocalStorageService } from './local-storage.service';

import { Observable } from 'rxjs/Rx';

@Injectable()
export class HttpClient {

    constructor(
        private http: Http,
        private utilityService: UtilityService,
        private localStorageService: LocalStorageService
    ) { }

    public generateRequestOptions(clientHeaders: string[]): RequestOptions {
        let headers: Headers = new Headers();
        let clientCred: string[] = this.utilityService.defaults.cred.concat( this.utilityService.defaults.assetCred );
        let tokens: any = this.localStorageService.storage || {};

        clientHeaders.forEach(cHeader => {
            if (cHeader === 'jsonContentType') return headers.set('Content-type', 'application/json');
            if (clientCred.indexOf(cHeader) > -1) return headers.set(cHeader, tokens[cHeader]);
        });

        let options: RequestOptions = new RequestOptions({ headers });
        return options;

    }

    public get(clientHeaders: string[], url: string): Observable<any> {
        let options: RequestOptions = this.generateRequestOptions(clientHeaders);
        return this.http.get(url, options);
    }

    public post(clientHeaders: string[], url: string, data?: any): Observable<any> {
        let options: RequestOptions = this.generateRequestOptions(clientHeaders);
        return this.http.post(url, data, options);
    }

    public put(clientHeaders: string[], url: string, data?: any): Observable<any> {
        let options: RequestOptions = this.generateRequestOptions(clientHeaders);
        return this.http.put(url, data, options);
    }

    public delete(clientHeaders: string[], url: string, data?: any): Observable<any> {
        let options: RequestOptions = this.generateRequestOptions(clientHeaders);
        options.body = data;
        return this.http.delete(url, options);
    }


}
