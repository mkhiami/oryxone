import { Component, OnInit } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Observable } from 'rxjs/Rx';
import { Http, Response, Headers, RequestOptions } from '@angular/http';

import { UtilityService } from '../../app/shared/services/utility.service';

import { Ife_record } from '../../app/shared/model/ife_record.model';

@Component({
  selector: 'page-record',
  templateUrl: 'record.component.html'
})
export class RecordComponent implements OnInit {

  constructor(
    public params: NavParams,
    public navCtrl: NavController,
    // public apiService: ApiService,
    public utilityService: UtilityService
  ) {}

  tab: string = undefined;
  item: Ife_record = new Ife_record(undefined,undefined,undefined,undefined,undefined,undefined,undefined,undefined,undefined,undefined,undefined,undefined,undefined,undefined,undefined,undefined,undefined,undefined,undefined,undefined,undefined,undefined,undefined,undefined,undefined,undefined,undefined,undefined,undefined,undefined);

  ngOnInit(): void {

    let _item = this.params.get( 'item' );
    this.tab = this.params.get( 'tab' );

    if ( this.tab.toLowerCase() === 'audio' ) _item.AUDIO_TRACK_DETAILS = this.removePageBreaks(_item.AUDIO_TRACK_DETAILS);
    this.item = _item;
    if ( !this.item || !this.tab ) {
      this.utilityService.openToast( 'Item not found' );
      this.navCtrl.pop();
    }

  }

  private removePageBreaks(_st: string): string[] {
    let __st = _st.replace(/\<br\s?\/\>/g, '_csv_divider_');
    let splitString: string[] = [];
    if ( __st.indexOf('_csv_divider_') > -1 ) {
      splitString = __st.split( '_csv_divider_' );
      return splitString.filter(item=> !!item && item.length);
    }
    return [ __st ];
  }

  public dismiss(event?: any): void {
    this.navCtrl.pop();
  }

}
