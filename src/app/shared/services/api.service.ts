import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import { HttpClient } from './http-client.service';


@Injectable()
export class ApiService {

  private globalConstantFields: string[] = [ '_id' ];
  private port: number = 1335;
  private hostname: string = 'oryx-one.fuego.qa';
  public host: string = `${location.protocol}//${this.hostname}:${this.port}`;

  constructor(
    private http: Http,
    private httpClient: HttpClient
  ) {};


  private getApiUrl(apiKey: string): string {
    return `${this.host}/api/${apiKey}`;
  }

  private getApiUrl_alt(apiKey: string): string {
    return `${this.host}/api/keys/${apiKey}`;
  }

  public get(clientHeaders: string[], apiKey: string, recordId: string): Observable<any> {

    return this.httpClient.get(clientHeaders, `${this.getApiUrl(apiKey)}/${recordId}`)
                .map((res: Response) => res.json())
                .catch((error: any) => Observable.throw(error.json().error || 'Server error'));

  }

  public getAll(clientHeaders: string[], apiKey: string, data?: any): Observable<any[]> {

    if ( (Object.keys(data||{}).length) ) {

      return this.httpClient.post(clientHeaders, this.getApiUrl_alt(apiKey), data)
                  .map((res: Response) => res.json())
                  .catch((error: any) => Observable.throw(error.json().error || 'Server error'));

    };

    return this.httpClient.get(clientHeaders, this.getApiUrl(apiKey))
              .map((res: Response) => res.json())
              .catch((error:any) => Observable.throw(error.json().error || 'Server error'));

  };

  public add(clientHeaders: string[], apiKey: string, body: Object): Observable<any[]> {

    let _body = JSON.stringify(body||'{}');

    return this.httpClient.post(clientHeaders, this.getApiUrl(apiKey), _body)
              .map((res: Response) => res.json())
              .catch((error:any) => Observable.throw(error.json().error || 'Server error'));

  };

  public update(clientHeaders: string[], apiKey: string, body: any): Observable<any[]> {

    let stringifyData = ((data)=> JSON.stringify(data||'{}'));

    let bodyId: string = body._id;
    this.globalConstantFields.forEach(field => delete(body[field]));

    return this.httpClient.put(clientHeaders, `${this.getApiUrl(apiKey)}/${bodyId}`, stringifyData(body))
              .map((res: Response) => res.json())
              .catch((error:any) => Observable.throw(error.json().error || 'Server error'));

  };

  public remove(clientHeaders: string[], apiKey: string, _id: string, data?: any): Observable<any[]> {

    return this.httpClient.delete(clientHeaders, `${this.getApiUrl(apiKey)}/${!data?_id:null}`, !!data?data:null)
              .map((res: Response) => res.json())
              .catch((error:any) => Observable.throw(error.json().error || 'Server error'));

  };

}
