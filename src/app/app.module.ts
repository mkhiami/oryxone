import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler, Config } from 'ionic-angular';

import { MyApp } from './app.component';
import { TabsPage } from '../pages/tabs/tabs';

import { TvPage } from '../pages/tv/tv';
import { AudioPage } from '../pages/audio/audio';
import { MoviesPage } from '../pages/movies/movies';

import { CatListingComponent } from '../pages/cat-listing/cat-listing.component';
import { RecordComponent } from '../pages/record/record.component';
import { HeaderComponent } from './shared/component/header.component';

import { ApiService } from './shared/services/api.service';
import { HttpClient } from './shared/services/http-client.service';
import { LocalStorageService } from './shared/services/local-storage.service';
import { UtilityService } from './shared/services/utility.service';


@NgModule({
  declarations: [
    MyApp,
    TabsPage,

    RecordComponent,
    CatListingComponent,
    TvPage,
    AudioPage,
    MoviesPage,
    HeaderComponent

  ],
  imports: [
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    TabsPage,

    RecordComponent,
    CatListingComponent,
    TvPage,
    AudioPage,
    MoviesPage,
    HeaderComponent

  ],
  providers: [
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ApiService,
    HttpClient,
    LocalStorageService,
    UtilityService
  ]
})
export class AppModule {}
