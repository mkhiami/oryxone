import { Component, Injectable } from '@angular/core';

@Injectable()
export class LocalStorageService {

    constructor() {}

    storage: any = {};
    isBrowserSupported: boolean = 'localStorage' in window;
    scriptEnabled: boolean = !0;
    lsNamespace: string = 'app-eventCache';
    lsTimeStampNamespace: string = 'timestamp';
    lsFetchNamespace: string = 'fetched';
    eventRegistered: any[] = [
        {
          key: 'sg-p1p-cached',
          fetchOnLoad: !0,
          isFetched: !1
        },
        {
          key: 'sg-p2p-cached',
          fetchOnLoad: !0,
          isFetched: !1
        },
        {
          key: 'rs-terminal',
          fetchOnLoad: !0,
          isFetched: !1
        }
    ];

    private typeOf(...args): boolean {

        if (!arguments[0]) return !1;
        return !arguments[1] ? Object.prototype.toString.call(arguments[0]).slice(8, -1).toLowerCase() : Object.prototype.toString.call(arguments[0]).slice(8, -1).toLowerCase() === arguments[1];

    }

    public init(): void {

        if ( !this.scriptEnabled || !this.isBrowserSupported ) return;
        (this.eventRegistered || []).forEach(item=> {

            if (!item.fetchOnLoad) return;
            return this.fetch(item.key) ? item.isFetched = !0 : !1;

        });

    }

    public fetch(x): boolean {

        let toNamespace: string = this.lsNamespace;
        let toLS: string = localStorage.getItem([x, toNamespace].join('-'));
        let _date: string = new Date().toString();

        if (!(toLS || '').length) {

            this.storage[x] = [];
            return !1;

        };

        this.storage[x] = JSON.parse(toLS);
        localStorage.setItem([x, toNamespace, this.lsFetchNamespace].join('-'), _date);
        return !0;

    }

    private mapEvent(x) {

      if ( !x ) return;
      let _f = this.eventRegistered.filter(item => item.key === x )||[];
      if ( _f.length > 1 ) return;
      return _f[0];

    }

    public set(x, o, cbs, isObject?): boolean {

        // x: category name
        // o: new object
        /**
          cb: callbacks:
            {
                beforeUpdate: function () {},
                afterUpdate: function () {}
            }
         */
       // (isObject? === !1), push to array
       // (isObject? === !0) update the namescape entirely

        let _isObject: boolean = isObject || !1;
        let toNamespace: string = this.lsNamespace;
        let mappedItem: any = this.mapEvent(x);
        let hasKeys: number = (Object.keys(cbs || {})).length;

        if (hasKeys)
            if (this.typeOf(cbs.beforeUpdate || {}, 'function'))
                if (!cbs.beforeUpdate.call(this, x, o)) return !1;

        if (!this.storage[x]) this.storage[x] = [];
        if (!_isObject) this.storage[x].push(o);
        if (_isObject) this.storage[x] = o;

        localStorage.setItem([x, toNamespace].join('-'), JSON.stringify(this.storage[x]));
        localStorage.setItem([x, toNamespace, this.lsTimeStampNamespace].join('-'), new Date().toString());

        if (hasKeys)
            if (this.typeOf(cbs.afterUpdate, 'function'))
                if (!cbs.afterUpdate.call(this, x, o)) return !1;

        return !0;

    }

    public flush(flushAll: boolean, clientKeys?: string[]) {

      /*
      this.namespace.flush(!1, [ 'sg-p1p-cached', 'g-p2p-cached' ])
      */

      let allKeys: string[] = Object.keys(localStorage);
      if ( flushAll ) return allKeys.forEach(key=> localStorage.removeItem(key));
      clientKeys.forEach(key=> {

        let generatedKeys: string[] = [];
        [
          this.lsNamespace,
          this.lsTimeStampNamespace,
          this.lsFetchNamespace,
          [this.lsNamespace, this.lsTimeStampNamespace].join('-'),
          [this.lsNamespace, this.lsFetchNamespace].join('-'),
        ]
        .map(namespace=> [key, namespace].join('-'))
        .forEach(namedKey=> localStorage.removeItem(namedKey))

        clientKeys.forEach(key=> this.storage[key] ? delete(this.storage[key]) : !1)

      })

    }

}
