import { Component, OnInit } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Observable } from 'rxjs/Rx';

import { RecordComponent } from '../record/record.component';
import { UtilityService } from '../../app/shared/services/utility.service';
import { ApiService } from '../../app/shared/services/api.service';
import { FadeTransition } from '../../app/shared/transition/fade-transition.component';

import { Ife_record } from '../../app/shared/model/ife_record.model';

@Component({
  selector: 'page-cat-listing',
  templateUrl: 'cat-listing.component.html'
})
export class CatListingComponent implements OnInit {

  constructor(
    public params: NavParams,
    public navCtrl: NavController,
    public apiService: ApiService,
    public utilityService: UtilityService
  ) {}

  tab: string = undefined;
  reqCat: string = undefined;
  apiKey: string = 'ife_records';
  items: Ife_record[] = [];
  displayedItems: Ife_record[] = [];
  treatFields: string[] = [ 'IFE_LANDING_IMG', 'AUDIO_COVER_IMAGE' ];
  reqHeaders: string[] = [ 'jsonContentType' ];
  batchNumber: number = 0;
  batchSize: number = 10;

  isLoading: boolean = !1;
  isEOF: boolean = !1;
  infiniteScrollInst: any = undefined;
  EofResetInterval: number = 12e5;
  loadingTimeoutInterval: number = 1000;

  ngOnInit() {

    this.tab = this.params.get('tab');
    this.reqCat = this.params.get( 'item' );
    if ( !this.reqCat || !this.tab ) return this.navCtrl.pop();

    this.fetchItems();

  }

  public checkRecord(item): void {
    this.navCtrl.push(RecordComponent, { item, tab: this.tab }, { animation: 'fade-animation', direction: 'forward' });
  }

  private fetchItems(): void {

    let mappedTab: any = {
      audio: 'Audio',
      tv: 'TV',
      movies: 'Movies'
    };

    this.getItems({
      query: {
        IFE_CATEGORY : this.reqCat,
        IFE_CATEGORYTYPE: mappedTab[this.tab.toLowerCase()],
       },
      batchSize: this.batchSize,
      batchNumber: this.batchNumber,
      populate : []
    }).subscribe(
      (res: Ife_record[]) => this.updateItems(res),
      (error: any) => this.fetchFailed(error)
    )
  }

  private fetchFailed(e?: any): void {
    console.log( 'fetchFailed fn call - error: ', e );
    this.isEOF = !0;
    this.finishLoading();
    this.utilityService.openToast( 'Server error. Try again later' );
    setTimeout(()=> this.isEOF = !1, 3e4);
  }

  private updateItems(res: Ife_record[]): void {

    if ( !res.length ) {
      this.isEOF = !0;
      // reset is endOfFile after 20 mins
      setTimeout(()=> this.isEOF = !1, this.EofResetInterval);
      return null
    };

    this.treatRecords(res).forEach(treatedItem=> this.displayedItems.push(treatedItem));
    this.items = this.displayedItems;

    ++this.batchNumber;
    this.finishLoading();

  }

  private finishLoading(): boolean {
    if ( !this.infiniteScrollInst ) return !1;
    this.isLoading = !1;
    this.infiniteScrollInst.complete();
    return !0;
  }

  private treatRecords(items: Ife_record[]): Ife_record[] {
    items.map(item=> {
      this.treatFields.forEach(field=> item[field] = this.utilityService.defaults.qaHost + item[field]);
      return item;
    })
    return items;
  }

  private getItems(data: any): Observable<Ife_record[]> {
    return this.apiService.getAll(this.reqHeaders, this.apiKey, data);
  }

  public dismiss(event?: any): void {
    this.navCtrl.pop();
  }

  public filterItems(event?: any): any {

    let toVal = event.target.value;
    if ( !toVal || toVal.trim() === '' ) return this.displayedItems = this.items;
    this.displayedItems = this.items.filter(item => {
      return item.IFE_TITLE.toLowerCase().indexOf( toVal.toLowerCase()) > -1
    })

  }

  public doInfinite(event?: any) {

    this.infiniteScrollInst = event;
    if ( this.isLoading ) return;
    this.isLoading = !0;
    setTimeout(() => this.fetchItems(), 1000);

  }

}
