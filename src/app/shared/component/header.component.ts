import { Component } from '@angular/core';

@Component({
  selector: 'page-header',
  template: `
  <ion-navbar>
    <ion-buttons start>
      <div class="image-wrapper">
        <img src="assets/img/oryx-110x25-white-transparent.png" width="110" height="25">
      </div>
    </ion-buttons>
    <ion-buttons left>
      <div class="image-wrapper">
        <img src="assets/img/qa-130x40-white-transparent.png" width="129" height="40">
      </div>
    </ion-buttons>
  </ion-navbar>
  `
})
export class HeaderComponent { }
