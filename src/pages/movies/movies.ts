import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { CatListingComponent } from '../cat-listing/cat-listing.component';
import { HeaderComponent } from '../../app/shared/component/header.component';

import { UtilityService } from '../../app/shared/services/utility.service';
import { ApiService } from '../../app/shared/services/api.service';
import { FadeTransition } from '../../app/shared/transition/fade-transition.component';

@Component({
  selector: 'page-movies',
  templateUrl: 'movies.html'
})
export class MoviesPage {

  constructor(
    public navCtrl: NavController,
    public apiService: ApiService,
    public utilityService: UtilityService
  ) {}

  tabName: string = 'movies';
  cats: string[] = this.utilityService.defaults.cats[this.tabName];

  public itemSelected(item): void {
    this.navCtrl.push(CatListingComponent, { item, tab: this.tabName }, { animation: 'fade-animation', direction: 'forward' });
  }

}
