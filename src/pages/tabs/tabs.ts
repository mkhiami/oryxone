import { Component } from '@angular/core';

import { TvPage } from '../tv/tv';
import { AudioPage } from '../audio/audio';
import { MoviesPage } from '../movies/movies';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {
  // this tells the tabs component which Pages
  // should be each tab's root Page
  tvRoot: any = TvPage;
  audioRoot: any = AudioPage;
  moviesRoot: any = MoviesPage;

  constructor() {}
}
