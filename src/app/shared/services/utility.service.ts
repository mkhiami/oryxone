import { Injectable } from '@angular/core';
import { Subject }    from 'rxjs/Subject';
import { ToastController } from 'ionic-angular';

function convertToHourFormat() {

    let sec_num: any = parseInt(this, 10); // don't forget the second param
    let hours: any = Math.floor(sec_num / 3600);
    let minutes: any = Math.floor((sec_num - (hours * 3600)) / 60);
    let seconds: any = sec_num - (hours * 3600) - (minutes * 60);

    if (hours < 10) hours = '0' + hours;
    if (minutes < 10) minutes = '0' + minutes;
    if (seconds < 10) seconds = '0' + seconds;

    let time: string = (hours === '00' ? '' : hours + ':') + minutes + ':' + seconds;
    return time;

}
declare global {
    interface String {
        toHHMMSS(): string;
        trim(): string;
        trimLeft(): string;
        trimRight(): string;
        firstCapital(): string;
        withCommas(): string;
        sterilise(): string;
    }
    interface Number {
        toHHMMSS(): string;
    }
}
Number.prototype.toHHMMSS = convertToHourFormat;
String.prototype.toHHMMSS = convertToHourFormat;

String.prototype.trim = function() {
    return (this || '').replace(/^\s*|\s*$/ig, '');
};
String.prototype.trimLeft = function() {
    return (this || '').replace(/^\s*/ig, '');
};
String.prototype.trimRight = function() {
    return (this || '').replace(/\s*$/ig, '');
};
String.prototype.firstCapital = function() {
    return (this || '')[0].toUpperCase() + (this || '').slice(1).toLowerCase();
};
String.prototype.withCommas = function() {
    if (Object.prototype.toString.call(this || arguments[0] || '').slice(8, -1).toLowerCase() !== 'string') return;
    return (this || arguments[0]).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
};
String.prototype.sterilise = function() {

    var entityMap = {
        "&": "&amp;",
        "<": "&lt;",
        ">": "&gt;",
        '"': '&quot;',
        "'": '&#39;',
        "/": '&#x2F;'
    };

    if (!this.length) return;
    if (Object.prototype.toString.call(this).slice(8, -1).toLowerCase() !== 'string') return;

    return (function(input) {
        return input.replace(/[&<>"'\/]/g, function(s) {
            return entityMap[s];
        });
    } (Array.prototype.join.call(this, '')));

};

@Injectable()
export class UtilityService {

    constructor(
      public toastCtrl: ToastController
    ) { }

    private notify = new Subject<any>();

    notifyObservable$ = this.notify.asObservable();

    public notifyOther(data: any) {
        if (data) this.notify.next(data);
    }

    defaults: any = {
      qaHost: 'http://www.qatarairways.com/',
        cats: {
          movies: [ 'Hollywood Premieres', 'All Time Hits', 'Hollywood Classics', 'Asia', 'Arabic Premieres', 'Action', 'Indian Sub Continent' ],
          audio: [ 'Hindi', 'Spotlight', 'Pop', 'Dance', 'Audio Books', 'Kids', 'Country', 'Classics', 'Gold', 'Movie Soundtracks', 'Rock', 'Arabic', 'Khaleeji', 'Jazz and Blues' ],
          tv: [ 'Documentaries', 'Drama', 'Arabic', 'Comedy', 'Asian', 'Business', 'Hindi', 'Kids', 'Lifestyle', 'Music & Arts', 'Sport', 'Travel', 'Urdu', 'European' ]
        },
        cred: [ 'sg-p1p-cached', 'sg-p2p-cached' ],
        assetCred: [ 'rs-terminal' ],
        isPublic: !1,
        accepted_types: [
            { type: 'image', ext: ['.jpeg', '.jpg', '.png', '.gif'] },
            { type: 'video', ext: ['.mp4', '.mov', '.3gp', '.webm', '.mkv'] },
            { type: 'audio', ext: ['.mp3', '.m4a', '.aac', '.wav', '.ts', '.flac'] },
            { type: 'application', ext: ['.pdf', '.doc', '.docx', '.xls', '.xlsx'] }
        ],
        currency: 'QAR',
        easingFunctions: {
            easeInOutCubic: function(t) {
                return t < .5 ? 4 * t * t * t : (t - 1) * (2 * t - 2) * (2 * t - 2) + 1
            },
            easeInOutQuint: function(t) {
                return t < .5 ? 16 * t * t * t * t * t : 1 + 16 * (--t) * t * t * t * t
            },
            easeInOutQuart: function(x, t, b, c, d) {
                if ((t /= d / 2) < 1) return c / 2 * t * t * t * t + b;
                return -c / 2 * ((t -= 2) * t * t * t - 2) + b;
            }
        },
        validationRule: {
            youtube: /(?:youtube\.com\/(?:[^\/]+\/.+\/|(?:v|e(?:mbed)?)\/|.*[?&]v=)|youtu\.be\/)([^"&?\/ ]{11})/i,
            vimeo: /vimeo.*\/(\d+)/,
            trim: /\<+.+?\>/gm,
            englishTitle: /([0-9a-zA-Z\s])+/,
            textOnly: /^([^\^*]+)$/,
            text: /^([^\^*]+)$/gm,
            registeredMobile: /^974(\d{8})$/,
            actCode: /^([\d]{6})$/,
            urlPattern: /\b(?:https?|ftp):\/\/[a-z0-9-+&@#\/%?=~_|!:,.;]*[a-z0-9-+&@#\/%=~_|]/,
            charCount: {
                en: /[\S]+/gm,
                ar: /[\S]+/gm
            },
            mobile: {
                en: /^(\+?[\d]{0,5})(\s)([\d\s]{8,9})$|^(\+?[\d]{9,13})$/,
                //ar : /^(\+?[\u0660-\u0669]+)(\s?)((?:\s)?[\u0660-\u0669]{8,9})$/
                ar: /^(\+?[\d]+)(\s)([\d\s]{8,9})$/

            },
            email: {
                en: /^[\u0000-\u007F]\S+@[\u0000-\u007F]\S+\.+[\u0000-\u007F]\S+$/,
                ar: /^[\u0000-\u007F]\S+@[\u0000-\u007F]\S+\.+[\u0000-\u007F]\S+$/
            },
            // for reference: not in use
            char: {
                en: /[\u0000-\u007F]/,
                ar: /[\u0600-\u06FF]/
            },
            // for reference: not in use
            num: {
                en: /[\u0030-\u0038]/,
                ar: /[\u0660-\u0669]/
            }
        }

    }

    checks: any = {
        polyfill: {},
        isPageVisible: 'hidden' in document ? !document['hidden'] : undefined, // initial call
        get pageVisibility() {

            var prefixes = ['webkit', 'moz', 'ms', 'o'],
                i = 0;
            if ('hidden' in document) return 'hidden';
            for (; i < prefixes.length; i++) {
                if ((prefixes[i] + 'Hidden') in document) return prefixes[i] + 'Hidden'
            };
            return null;

        },
        isPageOnline: 'onLine' in navigator ? navigator.onLine : !0, // initial call
        get pageConnectivity() {
            return 'onLine' in navigator ? navigator.onLine : !0;
        }
    }

    mobilecheck() {

        var mobb = (function() {
            var check = false;
            (function(a) {
                if (/(android|ipad|playbook|silk|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4)))
                    check = true
            })(navigator.userAgent || navigator.vendor);
            return check;
        } ());

        this.checks.mobilecheck = mobb;
        return this;

    }

    randomInRange(min: number, max: number) {
        return ~~(Math.random() * (max - min + 1)) + min;
    }

    extend(...args) {

        let extended = {};
        let deep = false;
        let i = 0;
        let length = arguments.length;
        let extend: any;

        if (Object.prototype.toString.call(arguments[0]) === '[object Boolean]') {
            deep = arguments[0];
            i++;
        };

        let merge = function(obj) {
            for (var prop in obj) {
                if (Object.prototype.hasOwnProperty.call(obj, prop)) {
                    if (deep && Object.prototype.toString.call(obj[prop]) === '[object Object]') {
                        extended[prop] = extend(true, extended[prop], obj[prop]);
                    } else {
                        extended[prop] = obj[prop];
                    };
                };
            };
        };

        for (; i < length; i++) {
            let obj = arguments[i];
            merge(obj);
        };

        return extended;
    }

    typeOf(...args) {

        if (!arguments[0]) return !1;
        return !arguments[1] ? Object.prototype.toString.call(arguments[0]).slice(8, -1).toLowerCase() : Object.prototype.toString.call(arguments[0]).slice(8, -1).toLowerCase() === arguments[1];

    }

    castedFieldChanged(model: any, field: string, event: any, type?: string) {

        let toElement = event.srcElement;
        let isChecked = toElement.checked;
        let checkValue = toElement.value;

        if (type === 'array') {

            if (!this.typeOf(model[field], 'array') || !model[field]) model[field] = [];
            if (isChecked) model[field].push(checkValue);
            if (!isChecked) model[field].forEach((val, i, a) => { if (val === checkValue) a.splice(i, 1) });

        } else if (type === 'string') {

            if (!this.typeOf(model[field], 'string') || !model[field]) model[field] = undefined;
            model[field] = isChecked ? checkValue : undefined
        }

    }

    generateRandomText(): string {
      return Math.random().toString(36).slice(2)
    }

    generateRandomNumber(): number {
      return Math.abs(~~(Math.random() * Math.random() * Math.random() * 1e25))
    }


    public openToast(msg: string, duration?: number): void {
      let toast = this.toastCtrl.create({
        message: msg,
        duration: duration||3000,
        showCloseButton: !0
      });
      toast.present();
    }

}
